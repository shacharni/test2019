@extends('layouts.app')

@section('content')

<h1>Update a todo</h1>
<form method='post' action="{{action('ProductController@update',$product->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
        <label> product to update</label>
        <label for ="name"> new name</label>
        <input type="text" class= "form-control" name="name" value="{{$product->name}}">
        <label for ="price"> price</label>
        <input type="text" class= "form-control" name="price" value="{{$product->price}}">
    </div>
    
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>
@endsection