@extends('layouts.app')

@section('content')

<h1>Create a new product</h1>
<form method='post' action="{{action('ProductController@store')}}">
    {{csrf_field()}}

    <div class="form-group">
    <h2>Add a product</h2>
        <label for ="name">name </label>
        <input type="text" class= "form-control" name="name">
        <label for ="price">price </label>
        <input type="double" class= "form-control" name="price">
        </div>
    <div class = "form-group">
        <input type="submit" class="form-control" name="submit" value="save">
    </div>
</form>
@endsection