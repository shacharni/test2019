
@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<h1> this is a products list</h1>
@can('manager')
<a href="{{route('products.create')}}">Create a new product</a>
@endcan
    <head>
    </head>
    <body>
    <table style="width:60%">
  <tr>
  <th></th>
    <th>name</th>
    <th>price</th>
    <th>username</th>
  </tr>
  @foreach($products as $product)
 
  <tr style="font-weight:bold">
 
  <tr>

 <td>   @if ($product->status)

           <input type = 'checkbox' id ="{{$product->id}}" checked  >
           <a href="{{route('computers.buy',['id' =>$product->id,'userid'=>$id])}}"> buy</a>
          @else
           <input type = 'checkbox' id ="{{$product->id}}" >
          
          
            
           
       @endif</td> 
       
        <td >{{$product->name}}</td>
        <td >{{$product->price}}</td>
        <td >{{$product->username}}</td>
        @can('manager')
        <td><a href="{{route('products.edit',$product->id)}}">edit</a> </td>

       <td>
       <form method='post' action="{{action('ProductController@destroy',$product->id)}}">
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type="submit" class="btn btn-link" name="submit" value="delete">
    </div>
</form>
</td>
@endcan
        </tr>
        @endforeach
        </table >
        
        </body>
        <script>
      $(document).ready(function(){
          $(":checkbox").click(function(event){
              console.log(event.target.id)
               $.ajax({
                   url: "{{url('products')}}"+'/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:"{{csrf_token()}}"}),
                   processData:false,
                  success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
      
   </script>  
        </html>

@endsection
