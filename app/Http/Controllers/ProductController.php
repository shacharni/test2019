<?php

namespace App\Http\Controllers;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use\Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::all();
        $id = Auth::id();
       // $users= User::all();
       
        $boss = DB::table('users')->where('id', $id)->first();
        $role = $boss->role;
        return view("products.index",['products'=>$products,'id' =>$id,'role'=> $role]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create products..");
             } 
  
        return view("products.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product=new Product();
        $product->name=$request->name;
        $product->price=$request->price;
        $product->save();
        return redirect('products');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
              }

        $product=Product::find($id);
       
        return view("products.edit",['product'=>$product]);
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       
          $product=Product::find($id);
          $product->update($request->all());
          return redirect ('products');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }

        $product=Product::find($id);
        if (!$product->user_id==Auth::id()) return(redirect('products'));
        $product->delete();
        return redirect ('products');

    }
    public function userid($id,$userid)
    {
        
        $boss = DB::table('users')->where('id',$userid)->first();
        $username = $boss->name;
        $product=Product::find($id);
        $product->username = $username;
        $product->user_id=$userid;
        $product->save();
        return redirect ('products');

    }
    public function nulluserid($id,$userid)
    {

        $product=Product::find($id);
        $product->user_id=NULL;
        $product->save();
        return redirect ('products');

    }
}
