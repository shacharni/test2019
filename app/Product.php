<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'price', 'status',
    ];
    public function user(){
        return $this-> belongsTo('App\User');
    }

}
