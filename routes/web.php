<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('products', 'ProductController')->middleware('auth');
//Route::get('userid/{id}/{userid}','ProductController@userid')->name('computers.buy');;
Route::get('products/{id}/{userid}','ProductController@userid')->name('computers.buy');
//Route::get('products/{id}/{userid}','ProductController@nulluserid')->name('computers1.buy');